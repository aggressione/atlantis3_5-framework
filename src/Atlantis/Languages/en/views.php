<?php





return [

	/*
	|--------------------------------------------------------------------------
	| General
	|--------------------------------------------------------------------------
	*/
	'' => 								'',
	'Save & Close' =>					'Save & Close',
	'Update' => 						'Update',
	'Edit' => 							'Edit',
	'Add' => 							'Add',
	'Delete' => 						'Delete',
	'Close' => 							'Close',
	'Save' => 							'Save',
	'Clone' => 							'Clone',
	'Restore' => 						'Restore',
	'Apply' => 							'Apply',
	'New' =>							'New',
	'Pattern' =>						'Pattern',
	'Patterns' =>						'Patterns',
	'Page' =>							'Page',
	'Tags' =>							'Tags',
	'Pages' =>							'Pages',
	'Media' =>							'Media',
	'Module' => 						'Module',
	'Modules' => 						'Modules',
	'Widget' => 						'Widget',
	'Widgets' => 						'Widgets',
	'is required' => 					'is required',
	'Title' => 							'Title',
	'ID' => 							'ID',
	'Url' => 							'Url',
	'Template' =>                       'Template',
	'Activate' =>                       'Activate',
	'Deactivate' => 					'Deactivate',
	'Description' => 					'Description',
	'Active' => 						'Active',
	'Updated at' =>						'Updated at',
	'Apply' => 							'Apply',
	'Done' => 							'Done',
	'Bulk actions' => 					'Bulk actions',
	'Show items' => 					'Show :items',


	/*
	|--------------------------------------------------------------------------
	| Nav
	|--------------------------------------------------------------------------
	*/
	'Settings' =>						'Settings',
	'Logout' =>							'Logout',
	'Edit this Page' =>					'Edit this Page',

	/*
	|--------------------------------------------------------------------------
	| Dashboard
	|--------------------------------------------------------------------------
	*/
	'Dashboard' =>						'Dashboard',
	'Welcome' =>						'Welcome, :name',
	'Search' =>							'Search',
	'fresh search' =>					'fresh search',
	'Latest Activity' =>				'Latest Activity',
	'new version available' =>			'New version available',

	/*
	|--------------------------------------------------------------------------
	| Pages
	|--------------------------------------------------------------------------
	*/
	'Delete Page' => 					               'Delete Page',
	'Clone Page' => 					               'Clone Page',
	'Preview Page' => 					               'Preview Page',
	'Restore Page' => 					               'Restore Page',
	'Edit Page' => 						               'Edit Page',
	'Do Nothing' => 					               'Do Nothing',
	'Prepend With' => 					                'Prepend With',
	'Append With' => 					                'Append With',
	'Version' =>						                'Version',
	'In Development' =>									'In Development',
	'By' =>								                'By',
	'Delete Category' =>	 			                'Delete Category',
	'Edit Category' =>	 				                'Edit Category',
	'Category Action' =>	 			                'Category Action',
	'Do Nothing' =>	 					                'Do Nothing',
	'Prepend With' =>	 				                'Prepend With',
	'Append With' =>	 				                'Append With',
	'times' =>							                'time|times',
	'Most Edited' => 					                'Most Edited',
	'Preview Page' => 					                'Preview Page',
	'Go to Pages' =>                                    'Go to Pages',
	'Add Category' =>                                   'Add Category',
	'Categories' =>										'Categories',
	'All Pages' =>										'All Pages',
	'Page Specific Styles' =>							'Page Specific Styles',
	'A3 Administration' =>                              'A3 Administration',
	'New Category' =>                                   'New Category',
	'Category Action' =>                                'Category Action',
	'Category Name' =>                                  'Category Name',
	'Category String' =>                                'Category String',
	'Category Default View' =>                          'Category Default View',
	'Category_Name_tip' =>                              'Category name can be arbitrary. it let\'s you organize pages in categories, as well as auto-generate urls.',
	'Category_String_tip' =>                            'Apply this string to the action. This string will be applied when creating a page to the page url , based on the action you have chosen above.',
	'Category_Action_tip' =>                            'This action defines what the url will become once a new page is being added to this category.',
	'Category_View_tip' =>                              'Default category view',
	'Add Page' =>                                       'Add Page',
	'Page Name' =>                                      'Page Name',
	'New Page' =>                                       'New Page',
	'Seo Title' =>                                      'Seo Title',
	'Page Category' =>                                  'Page Category',
	'Meta Keywords' =>                                  'Meta Keywords',
	'Parent Document' =>                                'Parent Document',
	'Meta Description' =>                               'Meta Description',
	'Page URL' =>                                       'Page URL',
	'Page Tags' =>                                      'Page Tags',
	'Page Template' =>                                  'Page Template',
	'Page Status' =>                                    'Page Status',
	'Meta Description' =>                              'Meta Description ',
	'expiration_To' =>                                  'expiration_To',
	'expiration_From' =>                                'expiration_From',
	'expiration_tip' =>                                 'expiration_tip',
	'Cache' =>                                          'Cache',
	'Expiration' =>                                     'Expiration',
	'Page Specific Scripts' =>                          'Page Specific Scripts',
	'Enable SSL for this page' =>                       'Enable SSL for this page',
	'Related Title' =>                                  'Related Title',
	'Related Image' =>                                  'Related Image',
	'Browse' =>                                         'Browse',
	'Remove' =>                                         'Remove',
	'Excerpt' =>                                        'Excerpt',
	'Notes' =>                                          'Notes',
	'Page Protected' =>                                 'Page Protected',
	'Enable page protection' =>                         'Enable page protection',
	'Canonical URL' =>                                  'Canonical URL',
	'page_name_tip' =>                                  'This is the name used to indentify the pattern in the CMS.',
	'seo_title_tip' =>                                  'This title appears between the <title> tags',
	'Page_URL_tip' =>                                   'The url address of the page',
	'expiration_tip' =>                                 'If you choose start and end date, page will be active only within that range. Leaving fields blank makes the page published indefinitely.',
	'canonical_tip' =>                                  'If you don’t add a value, Atlantis will generate canonical tag based on your page url.',
	'keywords_tip' =>                                   'If you leave this field blank, the global keywords will be used.',
	'metadescription_tip' =>                            'If you leave this field blank, the global description will be used.',
	'template_tip' =>                                   'Which display template the page should use.',
	'pagelang_tip' =>                                   'To create version in alternative language, please select from the dropdown.',
	'pagestatus_tip' =>                                 'Select publishing status for this page.',
	'canonical_tip' =>                                  'If you don’t add a value, Atlantis will generate canonical tag based on your page url.',
	'Include this page in site cache index' =>          'Include this page in site cache index',
	'Enter declarations one per line' =>                'Enter declarations one per line',
	'Shift+Enter for new row' =>                       	'Shift+Enter for new row ',
	'These styles will be loaded only for this page' => 'These styles will be loaded only for this page',
	'These scripts will be loaded only for this page' =>'These scripts will be loaded only for this page',
	'Page Patterns' =>									'Page Patterns',
	'Page specific' => 									'Page specific',
	'Common'  => 										'Common',
	'Excluded'  => 										'Excluded',
	'Add pattern to this page'  => 						'Add pattern to this page',
	'Remove pattern from this page'  => 				'Remove pattern from this page',
	'Password protected page' =>						'Password protected page',
	'Page is currently edited by' => 					'Page is currently edited by :name',


	/*
	|--------------------------------------------------------------------------
	| Patterns
	|--------------------------------------------------------------------------
	*/
	'Delete Pattern' => 				          		'Delete Pattern',
	'Clone Pattern' => 					          		'Clone Pattern',
	'Edit Pattern' => 					          		'Edit Pattern',
	'Restore Pattern' => 				           		'Restore Pattern',
	'This is revision' =>                               'This is revision :id and it\'s not the active version',
	'Make this version active' =>                       'Make this version active',
	'Go to Patterns' =>                                 'Go to Patterns',
	'Clone Pattern' =>                                  'Clone Pattern',
	'Versions' =>                                       'Versions',
	'Pattern Specific Attributes' =>                    'Pattern Specific Attributes',
	'Add New Field' =>                                  'Add New Field',
	'name' =>                                           'name',
	'Value' =>                                          'Value',
	'Delete Attribute' =>                               'Delete Attribute',
	'Awesome. I Have It.' =>                            'Awesome. I Have It.',
	'Attribute Name' =>                                 'Attribute Name',
	'Attribute Value' =>                                'Attribute Value',
	'Pattern Name' =>                                   'Pattern Name',
	'Resource URL' =>                                   'Resource URL',
	'Pattern output in' =>                              'Pattern output in',
	'Pattern View' =>                                   'Pattern View',
	'Pattern Tags' =>                                   'Pattern Tags',
	'Invalid field' =>                                  'Invalid field',
	'From' =>                                           'From',
	'To' =>                                             'To',
	'pattern_type_Text' => 								'Text',
	'pattern_type_View' => 								'View',
	'pattern_type_Resource' => 							'Resource',
	'Deactivated' => 									'Deactivated',
	'Latest Edited Patterns' =>                         'Latest Edited Patterns',
	'Are you sure you want to delete' =>                'Are you sure you want to delete',
	'Add Pattern' =>                                    'Add Pattern',
	'Go to Patterns' =>                                 'Go to Patterns',
	'New Pattern' =>                                    'New Pattern',
	'Pattern Specific Attributes' =>                    'Pattern Specific Attributes',
	'Add New Field' =>                                  'Add New Field',
	'Pattern Name' =>                                   'Pattern Name',
	'Pattern output in' =>                              'Pattern output in',
	'Pattern View' =>                                   'Pattern View',
	'URL Mask (one per line)' =>                        'URL Mask (one per line)',
	'All Patterns' =>		                            'All Patterns',
	'Delete Version' => 								'Delete Version',
	'Make Active Version' => 							'Make Active Version',
	'Leave blank to disable expiration' => 				'Leave blank to disable expiration',
	'This is the name used to indentify the pattern in the CMS' => 'This is the name used to indentify the pattern in the CMS',
	'Only if Resource is chosen above. Example : /patterns/feed/3' => 'Only if Resource is chosen above. Example : /patterns/feed/3',
	'To create version in alternative language, please select from the dropdown.' => 'To create version in alternative language, please select from the dropdown.',
	'Use this field when embedding the pattern in your template file. Don\'t include $ sign here.' => 'Use this field when embedding the pattern in your template file. Don\'t include $ sign here.',
	'Pattern is currently edited by' => 				'Pattern is currently edited by :name',

	/*
	|--------------------------------------------------------------------------
	| Modules
	|--------------------------------------------------------------------------
	*/
	'Namespace' => 						        'Namespace',
	'Status' => 						        'Status',
	'Author' => 						        'Author',
	'Repository' =>                             'Repository',
	'Installed Modules' =>                      'Installed Modules',
	'Available Modules' =>                      'Available Modules',
	'protected' =>                              'protected',
	'not active' =>                             'not active',
	'version' =>                                'version',
	'new version' =>                            'new version',
	'Uninstall' =>                              'Uninstall',
	'Install' =>                                'Install',
	'Create related pages and patterns' =>      'Create related pages and patterns',
	'needUpdate' =>                             'Some of installed modules needs to be updated to work properly',
	'no_mod_to_install' =>                      'There are no available modules for installation',
	'Modules Repository' =>                     'Modules Repository',
	'Add Key' =>                                'Add Key',
	'Keys' =>                                   'Keys',
	'Keys (One per line)' =>                    'Keys (One per line)',
	'Go to Modules' =>                          'Go to Modules',


	/*
	|--------------------------------------------------------------------------
	| Media
	|--------------------------------------------------------------------------
	*/
	'Delete Media' => 					'Delete Media',
	'Edit File' => 						'Edit File',
	'Delete File' => 					'Delete File',
	'Edit Gallery' => 					'Edit Gallery',
	'Delete Gallery' => 				'Delete Gallery',
	'Galleries' =>                      'Galleries',
	'Thumbnail' => 						'Thumbnail',
	'Name' => 							'Name',
	'Type' => 							'Type',
	'Size' => 							'Size',
	'Delete' => 						'Delete',
	'Add tags' => 						'Add tags',
	'Add to gallery' => 				'Add to gallery',
	'Add to existing gallery' => 		'Add to existing gallery',
	'Add to new gallery' => 			'Add to new gallery',
	'Delete File' => 					'Delete File',
	'View Full Image' => 				'View Full Image',
	'Add Media' =>                      'Add Media',
	'New Media' =>                      'New Media',
	'Alt' =>                            'Alt',
	'Anchor Link' =>                    'Anchor Link',
	'Resize' =>                         'Resize',
	'Caption' =>                        'Caption',
	'Weight' =>                         'Weight',
	'Credit' =>                         'Credit',
	'CSS' =>                            'CSS',
	'Upload' =>                         'Upload',
	'Inline styles' =>                  'Inline styles',
	'Wrap img src tag' =>               'Wrap img src tag',
	'Copy file path' =>                 'Copy file path',
	'Recrop Manually' =>                'Recrop Manually',
	'Open image in new tab' =>          'Open image in new tab',
	'galselecto_tip' =>                 'First Image in Gallery will be used as Featured Image',
	'New Gallery' =>                    'New Gallery',
	'Add Gallery' =>                    'Add Gallery',
	'Gallery Images' =>                 'Gallery Images',
	'Go to Media' =>                    'Go to Media',
	'Choose from existing media' =>     'Choose from existing media',
	'or upload new images' =>           'or upload new images',
	'plupload_notsupported' =>          'Your browser doesn\'t have Flash, Silverlight or HTML5 support.',
	'Copy file path' =>                 'Copy file path',
	'Open image in new tab' =>          'Open image in new tab',
	'Recrop Manually' =>                'Recrop Manually',
	'Add Gallery' => 					'Add Gallery',
	'Go to Media' => 					'Go to Media',
	'New Gallery' => 					'New Gallery',
	'Gallery Images' => 				'Gallery Images',
	'Edit Tags' => 						'Edit Tags',
	'Add Tags' => 						'Add Tags',
	'New Tags' => 						'New Tags',
	'Tags' => 							'Tags',
	'Edit Gallery' => 					'Edit Gallery',
	'Select gallery' => 				'Select gallery',
	'--Option--' => 					'--Option--',
	'Image files' => 					'Image files',
	'Zip files' => 						'Zip files',
	'File Versions' => 					'File Versions',
	'Original / Large' => 				'Original / Large',
	'Copied' => 						'Copied',
	'Change File' => 					'Change File',
	'Edit Media' => 					'Edit Media',
	'Path' => 							'Path',
	'Preview' => 						'Preview',
	'Page Preview Images' => 			'Page Preview Images',
	'Media is currently edited by' => 	'Media is currently edited by :name',
	'Copy code to clipboard' =>			'Copy code to clipboard',

	/*
	|--------------------------------------------------------------------------
	| User
	|--------------------------------------------------------------------------
	*/
	'User' => 							'User',
	'Users' => 							'Users',
	'Edit User' => 						'Edit User',
	'E-mail' =>							'E-mail',
	'Password' =>						'Password',
	'Language' =>						'Language',
	'Username' => 						'Username',
	'is required' => 					'is required',
	'Confirm New Password' =>			'Confirm New Password',
	'Editor' =>							'Editor',
	'admin-login' =>					'admin-login',
	'site-login' =>						'site-login',
	'Go to Users' =>					'Go to Users',
	'Widgets' => 						'Widgets',
	'Module' => 						'Module',
	'New User' =>                       'New User',
	'All Users' =>                      'All Users',
	'Add User' =>                       'Add User',
	'First Name' =>						'First Name',
	'Last Name' =>						'Last Name',
	'-- Show by role --' =>				'-- Show by role --',
	'Admin users' =>					'Admin users',
	'Site users' =>						'Site users',
    'Last login' =>                     'Last login',
    'IP' =>                             'IP',

	/*
	|--------------------------------------------------------------------------
	| Roles
	|--------------------------------------------------------------------------
	*/
	'Delete Role' =>					'Delete Role',
	'Edit Role' =>						'Edit Role',
	'Edit Role' =>						'Edit Role',
	'Go to Roles' =>					'Go to Roles',
	'Delete Role' =>					'Delete Role',
	'is required.' =>					'is required.',
	'Permissions' =>					'Permissions',
	'Module Permissions' =>				'Module Permissions',
	'Add Role' =>						'Add Role',
	'New Role' =>						'New Role',
	'Roles' =>							'Roles',
	'All Roles' =>						'All Roles',

	/*
	|--------------------------------------------------------------------------
	| Themes
	|--------------------------------------------------------------------------
	*/
	'not found in your theme\'s config file'    => 'not found in your theme\'s config file',
	'This theme is not valid'                   => 'This theme is not valid',
	'No Screenshot'                             => 'No Screenshot',
	'Available'                                 => 'Available',
	'Current theme'                             => 'Current theme',
	'Themes'                                    => 'Themes',
	'Theme'                                     => 'Theme',
	'Patterns variables'                        => 'Patterns variables',
	'Details'                                   => 'Details',
	'Themes list'                               => 'Themes list',

	/*
	|--------------------------------------------------------------------------
	| Config
	|--------------------------------------------------------------------------
	*/
	'Config' =>                                 'Config',
	'Domain Name' =>                            'Domain Name',
	'Site Name' =>                              'Site Name',
	'Shell_tip' =>                              'This view is the wrapper for all the other views it holds the common elements like head , body , etc /modules/atlantis/views/shell.php.',
	'Framework Version' =>                      'Framework Version: :version',
    'PHP Version' =>                            'PHP Version: :version',
	'CDN_tip' =>                                'in oder to make the CDN / Storage work, please setup ypur configuration in <pre>[root dir]/config/filesystem.php</pre>',
	'System Configuration' =>                   'System Configuration',
	'Include site name in title field' =>       'Include site name in title field',
	'System Config' =>                          'System Config',
	'Include site name in title field' =>       'Include site name in title field',
	'Default Keywords' =>                       'Default Keywords',
	'Default Language' =>                       'Default Language',
	'Default Description' =>                    'Default Description',
	'characters left' =>                        'characters left',
	'The Front end Shell View' =>               'The Front end Shell View',
	'Items per page on the admin screens' =>    'Items per page on the admin screens',
	'Cache lifetime (seconds)' =>               'Cache lifetime (seconds)',
	'Show shortcut bar' =>                      'Show shortcut bar',
	'Enable cache' =>                           'Enable cache',
	'Allowed max file size for upload in MB' => 'Allowed max file size for upload in MB',
	'Media upload directory' =>                 'Media upload directory',
	'Allowed image extensions' =>               'Allowed image extensions',
	'Allowed others extensions' =>              'Allowed others extensions',
	'Static Images' =>                          'Static Images',
	'Responsive Images' =>                      'Responsive Images',
	'use smart crop' =>                         'use smart crop',
	'Use face detection' =>                     'Use face detection',
	'Responsive Breakpoint sizes' =>            'Responsive Breakpoint sizes',
	'Default Styles' =>                         'Default Styles',
	'Default Scripts' =>                        'Default Scripts',
	'Excluded Scripts' =>                       'Excluded Scripts',
	'Default 404 view' =>                       'Default 404 view',
	'Amazon S3 and CloudFront' =>               'Amazon S3 and CloudFront',
	'S3 bucket url' =>                          'S3 bucket url',
	'CloudFront url' =>                         'CloudFront url',
	'Use S3 for file storage' =>                'Use S3 for file storage',
	'Use CloudFront as CDN' =>                  'Use CloudFront as CDN',
	'Delete local file after upload' =>         'Delete local file after upload',
	'Sync Files' =>                             'Sync Files',
	'Invalidate Files' =>                       'Invalidate Files',
	'site_name_tip' =>                          'Enabling this will include the site name title field',
	'Application / SiteName' =>                 'Application / SiteName',
	'Domain name' =>                            'Domain name',
	'Front-end default language' =>             'Front-end default language',
	'For example: png, jpeg' =>                 'For example: png, jpeg',
	'responsove_resize_tip' =>                  'Resize name/fullsize/thumbnail',
	'static_resize_tip' =>                      'Resize name/desktop/tablet/phone/thumbnail',
	'Smart_crop_tip' =>                         'Smart crop image thumbs with a fce detection',
	'Breakpoint_tip' =>                         'Breakpoint for responsive images. Ex: 1200/800',
	'Enable SSL' => 							'Enable SSL',

	/*
	|--------------------------------------------------------------------------
	| Trash
	|--------------------------------------------------------------------------
	*/
	'Empty Trash' => 		'Empty Trash',
	'Trash' => 				'Trash',

	/*
	|--------------------------------------------------------------------------
	| Modals
	|--------------------------------------------------------------------------
	*/
	'Are you sure you want to delete' =>							'Are you sure you want to delete :object',
	'Are you sure you want to restore' =>							'Are you sure you want to restore :object',
	'Are you sure you want to delete forever' =>  					'Are you sure you want to delete forever :object',
	'Are you sure you want to delete version' =>					'Are you sure you want to delete version :id',
	'Are you sure you want to remove all items from trash?' =>		'Are you sure you want to remove all items from trash?',
	'Are you sure you want to add' =>								'Are you sure you want to add :object',
	'Are you sure you want to remove' =>							'Are you sure you want to remove :object',
	'Remove Pattern' =>												'Remove Pattern',
	'Add Pattern' =>												'Add Pattern',

	/*
	|--------------------------------------------------------------------------
	| Helpers
	|--------------------------------------------------------------------------
	*/
	'Select version' =>                                 'Select version',
	'Close modal' =>                                    'Close modal',
	'Uninstall Module' =>                               'Uninstall Module',
	'Are you sure you want to uninstall' =>             'Are you sure you want to uninstall',
	'Are you sure you want to install' =>             	'Are you sure you want to install',
	'Add Directories (one per line)' =>                	'Add Directories (one per line) ',
	'is required.' =>                                   'is required.',
	'Please, wait while directories are saving...' =>   'Please, wait while directories are saving...',
	'Done!' =>                                          'Done!',
	'Error!' =>                                         'Error!',
	'Finalizing...' =>                                  'Finalizing...',
	'Syncing...' =>                                     'Syncing...',
	'Oops, something went wrong!' =>                    'Oops, something went wrong!',
	'Path to Files (one per line)' =>                  	'Path to Files (one per line) ',
	'Invalidate' =>                                     'Invalidate',
	'Install Module' =>                                 'Install Module',
	'Download and Install' =>                           'Download and Install',
	'Are you sure you want to deactivate' =>            'Are you sure you want to deactivate',
	'Deactivate Theme' =>                               'Deactivate Theme',
	'Creating Resource' =>                              'Creating Resource',
	'Pages:' =>                                         'Pages:',
	'Patterns:' =>                                      'Patterns:',
	'Create' =>                                         'Create',
	'Page Exist' =>                                     'Page Exist',
	'Clone Pattern' =>                                  'Clone Pattern',
	'Clone Name' =>                                     'Clone Name',
	'Clone Page' =>                                     'Clone Page',
	'Clone Url' =>                                      'Clone Url',
	'Activate Theme' =>                                 'Activate Theme',
	'Are you sure you want to activate' =>              'Are you sure you want to activate',
	'Add/Edit Files' =>                                 'Add/Edit Files',
	'Choose from existing media' =>                     'Choose from existing media',
	'or upload new files' =>                            'or upload new files',
	'Uploader' =>                                       'Uploader',
	'Files' =>                                          'Files',
	'ID' =>                                            	'ID',
	'Select File' =>                                    'Select File',
	'Image files' =>                                    'Image files',
	'Zip files' =>                                      'Zip files',
	'Gallery' =>                                        'Gallery',
	'actions' =>                                        'actions',
	'Edit This Gallery' =>                              'Edit This Gallery',
	'Create New Gallery' =>                             'Create New Gallery',
	'Refresh Gallery List' =>                           'Refresh Gallery List',
	'No data available in table' =>                     'No data available in table',
	'Showing _START_ to _END_ of _TOTAL_ entries' =>    'Showing :START to :END of :TOTAL entries',
	'Showing 0 to 0 of 0 entries' =>                    'Showing 0 to 0 of 0 entries',
	'(filtered from _MAX_ total entries)' =>            '(filtered from :MAX total entries)',
	'Show _MENU_ entries' =>                            'Show _MENU_ entries',
	'Loading...' =>                                     'Loading...',
	'Processing...' =>                                  'Processing...',
	'No matching records found' =>                      'No matching records found',
	'First' =>                                          'First',
	'Last' =>                                           'Last',
	'Next' =>                                           'Next',
	'Previous' =>                                       'Previous',
	': activate to sort column ascending' =>            ': activate to sort column ascending',
	': activate to sort column descending' =>           ': activate to sort column descending',
	'Reorder' =>                                        'Reorder',
	'This module will attempt to create the following pages and patterns for you' =>					'This module will attempt to create the following pages and patterns for you',
	'This module is trying to create the following pages that already exist in your installation' =>	'This module is trying to create the following pages that already exist in your installation',
	'First Image in Gallery will be used as Featured Image' =>											'First Image in Gallery will be used as Featured Image',
	'from LOCAL to S3' =>											'from LOCAL to S3',
	'from S3 to LOCAL' =>											'from S3 to LOCAL',
	'Folder can not be created' => 									'Folder :foldername can not be created',
	'Download failed' =>											'Download failed',
	'Module folder not found' =>									'Module folder not found',
	'Setup\.php file not found' => 									'Setup\.php file not found',
	'Invalid module path' =>										'Invalid module path',
	'Module namespace not found' =>									'Module namespace not found',
	'Namespace already exists' =>									'Namespace :namespace already exists',
	'The module was successfuly uninstalled. Please delete module folder from atlantis' => 'The module was successfuly uninstalled. Please delete module folder from atlantis',
	'Uninstall is not completed' =>									'Uninstall is not completed',

];
