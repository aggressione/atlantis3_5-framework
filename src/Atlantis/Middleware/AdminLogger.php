<?php

namespace Atlantis\Middleware;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class AdminLogger {
    public function handle(Request $request, \Closure $next) {

        if (env('ADMIN_LOGGER') !== true) {
            return $next($request);
        }

        $filename = 'admin_' . time() . '.log';

        /**
         * check filename size
         */
        $disk = Storage::build([
            'driver' => 'local',
            'root' => storage_path('logs')
        ]);

        foreach ($disk->allFiles() as $file) {

            if (Str::startsWith($file, 'admin_') && $disk->size($file) < 50000000) { //50mb max per file
                $filename = $file;
            }
        }

        $postData = $request->all();
        
        if (isset($postData['password'])) {
            $postData['password'] = $this->maskPostData($postData['password']);
        }

        /**
         * add log to the file
         */
        Log::build([
            'driver' => 'single',
            'path' => storage_path('logs/' . $filename),
        ])->info(json_encode([
            'userId' => auth()->user()->id ?? null,
            'userEmail' => auth()->user()->email ?? null,
            'method' => $request->method(),
            'url' => $request->fullUrl(),
            'params' => $postData,
            'ip' => $request->ip(),
            'session' => $request->getSession()->all(),
            'cookie' => $request->cookie()
        ], JSON_PRETTY_PRINT));

        return $next($request);
    }

    private function maskPostData($string, $mask = '*') {
        $len = Str::length($string);

        $masked = '';
        for ($i = 0; $i < $len; $i++) {
            $masked .= $mask;
        }
        return $masked;
    }
}
