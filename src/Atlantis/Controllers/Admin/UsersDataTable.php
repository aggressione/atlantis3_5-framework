<?php

namespace Atlantis\Controllers\Admin;

use Atlantis\Models\User;
use Illuminate\Support\Facades\DB;

class UsersDataTable implements \Atlantis\Helpers\Interfaces\DataTableInterface {

  public function __construct() {

    if (\Auth::check() === false) {

      return response()->json([]);
    }
    if (auth()->user() != NULL) {
      \App::setLocale(auth()->user()->language);
    }
  }

  public function columns() {

    return [
        [
            'title' => '<span class="fa fa-check-square-o select-all"></span>',
            'class-th' => 'checkbox no-sort',
            'class-td' => 'checkbox',
            'key' => 'checkbox',
            'order' => [
                'sorting' => FALSE,
                'order' => 'ASC'
            ]
        ],
        [
            'title' =>  trans('admin::views.ID'),
            'class-th' => '', // class for <th>
            'class-td' => 'id', // class for <td>
            'key' => 'id', // db column name
            'order' => [
                'sorting' => TRUE, // only one column have TRUE
                'order' => 'desc'
            ]
        ],
        [
            'title' =>  trans('admin::views.Name'),
            'class-th' => '',
            'class-td' => 'name',
            'key' => 'name',
            'order' => [
                'sorting' => FALSE,
                'order' => 'ASC'
            ]
        ],
        [
            'title' =>  trans('admin::views.E-mail'),
            'class-th' => '',
            'class-td' => '',
            'key' => 'email',
            'order' => [
                'sorting' => FALSE,
                'order' => 'ASC'
            ]
        ],
        [
            'title' =>  trans('admin::views.Updated at'),
            'class-th' => '',
            'class-td' => 'template-class',
            'key' => 'updated_at',
            'order' => [
                'sorting' => FALSE,
                'order' => 'ASC'
            ]
        ],
        [
            'title' =>  trans('admin::views.Last login'),
            'class-th' => '',
            'class-td' => 'template-class',
            'key' => 'last_login',
            'order' => [
                'sorting' => FALSE,
                'order' => 'ASC'
            ]
        ],
        [
            'title' =>  trans('admin::views.IP'),
            'class-th' => '',
            'class-td' => 'template-class',
            'key' => 'ip',
            'order' => [
                'sorting' => FALSE,
                'order' => 'ASC'
            ]
        ]
    ];
  }

  /**
   * Fill array or return empty.
   *
   * @return array
   */
  public function bulkActions() {

    return [
        'url' => 'admin/users/bulk-action',
        'actions' => [
            [
                'name' => trans('admin::views.Delete'),
                'key' => 'bulk_delete'
            ]
        ]
    ];
  }

  public function customFilters() {

     return [
        [
        //'label' => 'Search By Role',
        //'help_text' => 'Search By Role',
        'size' => '2', //number of foundation grid columns
        'key' => 'role_id',//required
        'options' => //required
          [
            '' => trans('admin::views.-- Show by role --'),
            '1' => trans('admin::views.Admin users'),
            '2' => trans('admin::views.Site users')
          ]
      ]
    ];

  }

  public function getData(\Illuminate\Http\Request $request) {

    $model = DB::table('users');
     /*
     * Custom Filter
     */

      if ( !empty($request->get('custom_filter')) && $request->get('custom_filter')[0]['value'] != '') {
        $userIdsByRole = DB::table('roles_users')
        ->where($request->get('custom_filter')[0]['name'], '=',  $request->get('custom_filter')[0]['value'])
        ->get()->pluck('user_id')->toArray();
        $model->whereIn('id', $userIdsByRole);
      }


    /*
     * SEARCH
     */
    if (isset($request->get('search')['value']) && !empty($request->get('search')['value'])) {
      $search = $request->get('search')['value'];
      $model->where(function($query) use ($search){
        $query->orWhere('users.id', 'LIKE', '%' . $search . '%');
        $query->orWhere('users.name', 'LIKE', '%' . $search . '%');
        $query->orWhere('users.email', 'LIKE', '%' . $search . '%');
       });
    }

    /*
     * Count filtered data without LIMIT and OFFSET
     */
    $modelWhitoutOffset = $model;
    $count = $modelWhitoutOffset->count();

    /*
     * OFFSET and LIMIT
     */
    $model->take($request->get('length'));
    $model->skip($request->get('start'));

    /*
     * ORDER BY
     */
    if (isset($request->get('order')[0]['column']) && isset($request->get('order')[0]['dir'])) {

      $column = $request->get('order')[0]['column'];
      $dir = $request->get('order')[0]['dir'];
      $columns = $request->get('columns');

      $model->orderBy($columns[$column]['data'], $dir);
    }

    /*
     * Get filtered data
     */
    $modelWithOffset = $model->get();

    $data = array();

    foreach ($modelWithOffset as $k => $obj) {

      $data[$k] = [
          'checkbox' => '<span data-atl-checkbox>' . \Form::checkbox($obj->id, NULL, FALSE, ['data-id' => $obj->id]) . '</span>',
          'id' => $obj->id,
          'name' => $this->nameTd($obj),
          'email' => $obj->email,
          'updated_at' => $obj->updated_at,
          'last_login' => $obj->last_login,
          'ip' => $obj->ip
      ];
    }

    return response()->json([
                'drow' => $request->get('draw'),
                'recordsTotal' => User::count(),
                'recordsFiltered' => $count,
                'data' => $data
    ]);
  }

  private function nameTd($obj) {

    if (UsersController::canDeleteUser($obj->id)) {
      $delIcon = '<a data-open="deleteUser' . $obj->id . '" data-tooltip aria-haspopup="true" data-disable-hover="false" tabindex="1" data-alt-text="'.trans('admin::views.Delete User'). '" title="'.trans('admin::views.Delete User'). '" class="icon icon-Delete top "></a>';
    } else {
      $delIcon = '';
    }

    return '<span class="tags hidden">tags</span>
                    <a class="item" href="admin/users/edit/' . $obj->id . '">' . $obj->name . '</a>
                    <span class="actions">
                      <a data-tooltip data-alt-text="'.trans('admin::views.Edit User'). '" title="'.trans('admin::views.Edit User'). '" href="admin/users/edit/' . $obj->id . '" class="icon icon-Edit top"></a> ' .
            $delIcon . '</span>' .
            \Atlantis\Helpers\Modal::set('deleteUser' . $obj->id, trans('admin::views.Delete User'), trans('admin::views.Are you sure you want to delete',['object' => $obj->name ]), trans('admin::views.Delete'), '/admin/users/delete/' . $obj->id);
  }

  /**
   * Add class to <table></table> tag
   *
   */
  public function tableClass() {
    return NULL;
  }
}
