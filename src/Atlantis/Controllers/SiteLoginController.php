<?php

namespace Atlantis\Controllers;

use Atlantis\Http\Controllers\Controller;
use Illuminate\Support\MessageBag as MessageBag;

class SiteLoginController extends Controller {

    public function index(\Request $request) {

        if (\Auth::check() && \Auth::user()->hasRole('site-login')) {

            return \Redirect(config('page-protected.route_after_login'));

        } else {

            if ($request::isMethod('post')) {

                $eventData = [
                    'username' => request()->get('username')
                ];

                \Event::dispatch('site.authAttempt', $eventData);

                return $this->authAttempt([
                    'name' => request()->get('username'),
                    'password' => request()->get('password')
                ]);

            } else {

                return $this->getLoginView($request);
            }
        }
    }

    public function logout() {

        \Event::dispatch('site.logout', ['user' => \Auth::user()]);

        \Auth::logout();

        return \Redirect(config('page-protected.route_after_logout'));
    }

    /**
     * Return login view
     */
    private function getLoginView(\Request $request) {

        $pageController = new PageController(new \Atlantis\Models\Repositories\PageRepository(), new \Atlantis\Models\Repositories\PatternRepository());

        return $pageController->index($request);


    }

    /**
     * Try to log in user
     */
    private function authAttempt($credentials) {

        if (\Auth::attempt($credentials, false)) {

            \Event::dispatch('site.login', ['user' => \Auth::user()]);
            return \Redirect(config('page-protected.route_after_login'));
        } else {

            $errors = new MessageBag(['password' => ['Username or Password is Invalid.']]);
            if (config('page-protected.on_error_redirect_to') == '{{back}}' || empty(config('page-protected.on_error_redirect_to'))) {
                return \Redirect::back()->withErrors($errors)->withInput(request()->except('password'));
            } else {
                return redirect(config('page-protected.on_error_redirect_to'))->withErrors($errors)->withInput(request()->except('password'));
            }
        }
    }
}
