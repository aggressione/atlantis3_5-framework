<?php

namespace Atlantis\Controllers;

use Atlantis\Http\Controllers\Controller;

class DataTableResolver extends Controller {

  public function resolve() {

    $namespaceClass = rawurldecode(request()->get('namespaceClass'));

    $dataTableClass = new $namespaceClass();

    return $dataTableClass->getData(request());
  }

  public function reorderData() {

    $namespaceClass = rawurldecode(request()->get('namespaceClass'));

    $dataTableClass = new $namespaceClass();

    return $dataTableClass->rowsReorder(request());
  }

}
