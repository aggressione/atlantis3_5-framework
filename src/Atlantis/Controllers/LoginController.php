<?php

namespace Atlantis\Controllers;

use Atlantis\Http\Controllers\Controller;
use Illuminate\Support\MessageBag as MessageBag;
use Atlantis\Models\Repositories\MultiSitesSessionsRepository;

class LoginController extends Controller {


    /**
     * MULTI SITES
     *
     * Set login session from master site
     */
    public function setLoginSession(\Request $request) {

        $multiSitesConfig = config('multi-sites');

        $masterSite = \Atlantis\Helpers\Tools::getMasterSite($multiSitesConfig);

        if ($masterSite != NULL) {

            $url = $masterSite['domain'] . '/get-logged-user';
            $myvars = 'ip=' . \Request::ip()
                . '&logged_session=' . request()->get('session')
                . '&key=' . $multiSitesConfig['key'];

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $myvars);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $response = curl_exec($ch);

            \Auth::logout();

            if (!empty($response)) {

                \Auth::loginUsingId($response, 1);
            }
        }

        return \Redirect("admin");

    }

    /**
     * MULTI SITES
     *
     * Check if master site have valid session in DB
     * and return logged user id.
     *
     * This function wait cURL postRequest.
     */

    public function getLoggedUser(\Illuminate\Http\Request $request) {

        //$session = $request->get('logged_session');
        $ip = $request->get('ip');
        $multi_sites_key = $request->get('key');

        $multiSitesConfig = config('multi-sites');

        if ($multiSitesConfig['key'] == $multi_sites_key) {

            $multisitesSessions = MultiSitesSessionsRepository::getSessionByIP($ip);

            if (!$multisitesSessions->isEmpty()) {

                return $multisitesSessions->first()->logged_user;
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }

    }
}
