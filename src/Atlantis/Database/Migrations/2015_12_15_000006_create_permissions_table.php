<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermissionsTable extends Migration {

    /**
     * Run the migrations.
     */
    public function up() {

        if (Schema::hasTable('permissions')) {
            return;
        }

        Schema::create('permissions', function (\Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('id');
            $table->integer("role_id");
            $table->string("type", 255);
            $table->text("value");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down() {
        Schema::dropIfExists('permissions');
    }

}
