<?php

use Illuminate\Database\Migrations\Migration;

class AddUserLastLogin extends Migration {

  /**
   * Run the migrations.
   */
  public function up() {
    if (!Schema::hasColumn('users', 'last_login')) {
      Schema::table('users', function(\Illuminate\Database\Schema\Blueprint $table) {
        $table->dateTime("last_login")->nullable();
      });
    }

      if (!Schema::hasColumn('users', 'ip')) {
          Schema::table('users', function(\Illuminate\Database\Schema\Blueprint $table) {
              $table->string("ip")->nullable();
          });
      }
  }

  /**
   * Reverse the migrations.
   */
  public function down() {
    if (Schema::hasColumn('users', 'last_login')) {
      Schema::table('users', function(\Illuminate\Database\Schema\Blueprint $table) {
        $table->dropColumn("last_login");
      });
    }

      if (Schema::hasColumn('users', 'ip')) {
          Schema::table('users', function(\Illuminate\Database\Schema\Blueprint $table) {
              $table->dropColumn("ip");
          });
      }
  }


}
