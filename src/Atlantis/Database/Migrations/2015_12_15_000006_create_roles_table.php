<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration {

    /**
     * Run the migrations.
     */
    public function up() {

        if (Schema::hasTable('roles')) {
            return;
        }

        Schema::create('roles', function (\Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('id');
            $table->string("name", 255);
            $table->text("description");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down() {
        Schema::dropIfExists('roles');
    }

}
