<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatternsMasksTable extends Migration {

    /**
     * Run the migrations.
     */
    public function up() {

        if (Schema::hasTable('patterns_masks')) {
            return;
        }

        Schema::create('patterns_masks', function (\Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('id');
            $table->integer("pattern_id")->nullable();
            $table->string("mask", 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down() {
        Schema::dropIfExists('patterns_masks');
    }

}
