<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class CreateModulesTable extends Migration {

    /**
     * Run the migrations.
     */
    public function up() {

        if (Schema::hasTable('modules')) {
            return;
        }

        Schema::create('modules', function (\Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('id');
            $table->string("name", 255)->nullable();
            $table->string("author", 255)->nullable();
            $table->string("version", 45)->nullable();
            $table->string("namespace", 255)->nullable();
            $table->string("path", 255)->nullable();
            $table->string("provider", 255)->nullable();
            $table->string("adminURL", 255)->nullable();
            $table->string("icon", 255)->nullable();
            $table->text("extra")->nullable();
            $table->integer("active")->default(1);
            $table->text("description")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down() {
        Schema::dropIfExists('modules');
    }

}
