<?php

use Illuminate\Database\Migrations\Migration;

class AddModulesPublishedAssets extends Migration {

  /**
   * Run the migrations.
   */
  public function up() {
    if (!Schema::hasColumn('modules', 'published_assets')) {
      Schema::table('modules', function(\Illuminate\Database\Schema\Blueprint $table) {
        $table->boolean("published_assets")->default(false)->after('active');
      });
    }
  }

  /**
   * Reverse the migrations.
   */
  public function down() {
    if (Schema::hasColumn('modules', 'published_assets')) {
      Schema::table('modules', function(\Illuminate\Database\Schema\Blueprint $table) {
        $table->dropColumn("published_assets");
      });
    }
  }


}
