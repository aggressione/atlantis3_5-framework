<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTagsTable extends Migration {

    /**
     * Run the migrations.
     */
    public function up() {

        if (Schema::hasTable('tags')) {
            return;
        }

        Schema::create('tags', function (\Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('id');
            $table->string("resource", 255)->nullable();
            $table->integer("resource_id")->nullable();
            $table->string("tag", 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down() {
        Schema::dropIfExists('tags');
    }

}
