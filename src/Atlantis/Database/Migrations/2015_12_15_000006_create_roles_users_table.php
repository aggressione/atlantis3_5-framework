<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesUsersTable extends Migration {

    /**
     * Run the migrations.
     */
    public function up() {

        if (Schema::hasTable('roles_users')) {
            return;
        }

        Schema::create('roles_users', function (\Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('id');
            $table->integer("user_id");
            $table->integer("role_id");


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down() {
        Schema::dropIfExists('roles_users');
    }

}
