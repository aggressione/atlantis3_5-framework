<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMultisitesSessionsTable extends Migration {

    /**
     * Run the migrations.
     */
    public function up() {

        if (Schema::hasTable('multisites_sessions')) {
            return;
        }

        Schema::create('multisites_sessions', function (\Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('id');
            $table->integer("logged_user");
            $table->string("key", 255)->nullable();
            $table->string("value", 255)->nullable();
            $table->string("ip", 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down() {
        Schema::dropIfExists('multisites_sessions');
    }

}
