<?php

namespace Atlantis\Http\Controllers\Auth;

use Atlantis\Http\Controllers\Controller;
use Atlantis\Middleware\AdminLogger;

class AuthController extends  Controller {
    public function __construct() {
        $this->middleware(AdminLogger::class);
        if (request()->method() === 'POST') {
            $this->middleware('throttle: 6,1');
        }
    }
}
