<?php

namespace Atlantis\Http\Controllers\Auth;

use Atlantis\Http\Controllers\Controller;
use Atlantis\Http\Requests\Auth\LoginRequest;
use Atlantis\Models\Repositories\MultiSitesSessionsRepository;
use Atlantis\Providers\RouteServiceProvider;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\MessageBag as MessageBag;

class AuthenticatedSessionController extends AuthController {
    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create() {

        if (env('MULTI_SITES', false)) {
            return $this->multiLogin();
        } else {
            return $this->regularLogin();
        }

    }

    /**
     * Handle an incoming authentication request.
     *
     * @param LoginRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LoginRequest $request) {
        $request->authenticate();

        auth()->user()->update(['last_login' => Carbon::now()->toDateTimeString(), 'ip' => $request->ip()]);

        $request->session()->regenerate();

        $login_session = $this->getLoginSession();

        if (env('MULTI_SITES', false) && \Atlantis\Helpers\Tools::isMasterSite(config('multi-sites'))) {
            MultiSitesSessionsRepository::setSession($login_session, 1, request()->ip(), auth()->user()->id);
        }

        if (request()->get('redirect') != NULL) {

            if (request()->get('with_auth_session') != NULL) {

                return \Redirect(urldecode(request()->get('redirect')) . '?session=' . $login_session);
            }

            return \Redirect(urldecode(request()->get('redirect')));
        }

        return $this->redirectToAdmin();
    }

    /**
     * Destroy an authenticated session.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request) {

        $redirect = \Redirect(\URL::to("/"));

        if (env('MULTI_SITES', FALSE)) {

            $multiSitesConfig = config('multi-sites');

            if (!\Atlantis\Helpers\Tools::isMasterSite($multiSitesConfig)) {

                $masterSite = \Atlantis\Helpers\Tools::getMasterSite($multiSitesConfig);

                if ($masterSite != NULL) {
                    /**
                     * Redirect to master site to sign out
                     */
                    $url = request()->root();
                    $redirect = \Redirect($masterSite['domain'] . "/admin/logout?redirect=" . urlencode($url));
                } else {
                    $errors = new MessageBag(['password' => ['Master site is not set on config/multi-sites.php']]);
                    $redirect = \Redirect::back()->withErrors($errors)->withInput(request()->except('password'));
                }
            } else {

                MultiSitesSessionsRepository::deleteSession($this->getLoginSession(), \Request::ip(), \Auth::user()->id);

                if (request()->get('redirect') != NULL) {
                    $redirect = \Redirect(urldecode(request()->get('redirect')));
                }
            }
        }

        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return $redirect;

    }

    /**
     * Regular/normal/standard login
     */
    private function regularLogin() {

        if (auth()->check() && auth()->user()->hasRole('admin-login')) {

            if (request()->get('redirect') != NULL) {

                if (request()->get('with_auth_session') != NULL) {

                    $login_session = $this->getLoginSession();
                    return \Redirect(urldecode(request()->get('redirect')) . '?session=' . $login_session);
                }

                return \Redirect(urldecode(request()->get('redirect')));
            }

            //redirect to admin/dashboard
            return $this->redirectToAdmin();
        }

        return $this->getLoginView();

    }

    /**
     * Return login view
     */
    private function getLoginView() {

        $_page = new \stdClass();

        $_page->seo_title = "Atlantis Login";

        $_page->tracking_header = "";

        \Event::dispatch("page.body_class", ['login-page', null, null, null]);

        $aParams = array();
        $aParams['_page'] = $_page;

        $urlQuery = '';

        if (request()->get('redirect') != NULL) {

            $urlQuery .= '?redirect=' . urlencode(request()->get('redirect'));

            if (request()->get('with_auth_session') != NULL) {
                $urlQuery .= '&with_auth_session=' . request()->get('with_auth_session');
            }
        }

        $aParams['urlQuery'] = $urlQuery;

        return view('atlantis::auth/login', $aParams);

    }

    private function redirectToAdmin() {

        if (isset(auth()->user()->id)) {

            $userPermissions = \Atlantis\Models\Repositories\PermissionsRepository::getAllPermissionsForUser(auth()->user()->id);
            $aUserPerm = array();
            foreach ($userPermissions as $user_perm) {
                $aUserPerm[] = $user_perm->type;
            }

            $aUserPerm = array_unique($aUserPerm);

            $routes = \Route::getRoutes();

            foreach ($routes as $route) {

                $action = $route->getAction();

                if (isset($action['identifier']) && isset($action['name']) && $this->isAllowed($aUserPerm, $action['identifier'])) {
                    \Event::dispatch('admin.login');
                    return \Redirect($action['menu_item_url']);
                }
            }
        }

        return \Redirect("admin/dashboard");
    }

    private function isAllowed($aUserPerm, $identifier) {

        $allow = FALSE;

        if (auth()->user()->hasRole('admin') || in_array($identifier, $aUserPerm)) {
            $allow = TRUE;
        }

        return $allow;
    }

    /**
     * MULTI SITES
     *
     * Get login session if this site is master
     */
    private function getLoginSession() {

        $sessions = \Illuminate\Support\Facades\Session::all();

        $login_session = NULL;

        foreach ($sessions as $key => $value) {
            if (stristr($key, "login_")) {
                $login_session = $key;
            }
        }

        return $login_session;
    }

    /**
     * MULTI SITES
     * Login with multi sites
     */
    private function multiLogin() {

        if (\Auth::check() && \Auth::user()->hasRole('admin-login')) {

            if (request()->get('redirect') != NULL) {

                if (request()->get('with_auth_session') != NULL) {

                    $login_session = $this->getLoginSession();

                    MultiSitesSessionsRepository::setSession($login_session, 1, request()->ip(), auth()->user()->id);

                    return \Redirect(urldecode(request()->get('redirect')) . '?session=' . $login_session);
                }

                return \Redirect(urldecode(request()->get('redirect')));
            }

            //redirect to admin/dashboard
            return $this->redirectToAdmin();
        } else {

            $multiSitesConfig = config('multi-sites');

            if (!\Atlantis\Helpers\Tools::isMasterSite($multiSitesConfig)) {

                $masterSite = \Atlantis\Helpers\Tools::getMasterSite($multiSitesConfig);

                if ($masterSite != NULL) {
                    /*
                     * Redirect to master site with user credentials
                     */
                    $url = request()->root() . '/set-login-session';
                    return \Redirect($masterSite['domain'] . "/admin?redirect=" . urlencode($url) . '&with_auth_session=1');
                } else {
                    $errors = new MessageBag(['password' => ['Master site is not set on config/multi-sites.php']]);
                    return \Redirect::back()->withErrors($errors)->withInput(request()->except('password'));
                }
            } else {

                return $this->regularLogin();
            }
        }
    }
}
