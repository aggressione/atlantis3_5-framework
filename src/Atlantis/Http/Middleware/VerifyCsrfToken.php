<?php

namespace Atlantis\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
    ];

    /**
     * Add URIs to the CSRF exception list.
     *
     * @param array $uris
     */
    public function addExcept(array $uris)
    {
        $this->except = array_merge($this->except, $uris);
    }
}
