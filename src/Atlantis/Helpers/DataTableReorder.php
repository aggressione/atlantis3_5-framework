<?php

namespace Atlantis\Helpers;

use Illuminate\Support\Arr;

class DataTableReorder {

  private $dataTableClass;
  private $dataTableClassNamespace;
  private $postParams = array();
  private $dataTableScript;

  public function __construct($dataTableClassNamespace, $postParams = array(), $dataTableScript = 'data-table-script-reorder') {

    $this->dataTableClassNamespace = $dataTableClassNamespace;

    $this->dataTableClass = new $dataTableClassNamespace();

    $this->postParams = $postParams;

    $this->dataTableScript = $dataTableScript;

    Assets::registerStyle('https://cdn.datatables.net/rowreorder/1.2.0/css/rowReorder.foundation.min.css');

    Assets::registerScript('vendor/atlantis-labs/atlantis3_5-framework/src/Atlantis/Assets/DataTables/media/js/jquery.dataTables.min.js');

    Assets::registerScript('https://cdn.datatables.net/rowreorder/1.2.0/js/dataTables.rowReorder.min.js');
  }

  public function create() {

    $aData = array();

    $aBulkActions = $this->dataTableClass->bulkActions();

    $aData['aBulkActions'] = array();

    if (!empty($aBulkActions)) {
      $aData['bulk_action_url'] = $aBulkActions['url'];
      $aData['aBulkActions'] = Arr::prepend($aBulkActions['actions'], ['name' => trans('admin::views.Bulk actions'), 'key' => 'bulk_none']);
    }

    $columns = $this->dataTableClass->columns();
    //var_dump(is_callable($this->dataTableClass->columns()));
    $aData['columns'] = $columns;

    $tableClass = $this->dataTableClass->tableClass();

    if ($tableClass != NULL) {
      $aData['tableClass'] = $tableClass;
    } else {
      $aData['tableClass'] = '';
    }

    $aData['url'] = url('/').'/datatable-proccessing/getdata';

    $aData['namespaceClass'] = rawurlencode($this->dataTableClassNamespace);

    $id = rand(0, 9999);

    $aData['table_id'] = 'tdid' . $id;

    $aData['postParams'] = $this->postParams;

    $admin_items_per_page = intval(config('atlantis.admin_items_per_page'));

    if ($admin_items_per_page <= 0) {
      $admin_items_per_page = 25;
    }

    $aData['lengthMenu'] = $this->lengthMenu($admin_items_per_page);

    $aData['admin_items_per_page'] = $admin_items_per_page;

    Assets::registerJS(view('atlantis-admin::helpers/' . $this->dataTableScript, $aData));

    return view('atlantis-admin::helpers/data-table-reorder', $aData);
  }

  private function lengthMenu($admin_items_per_page) {

    $aItems = [25, 50, 100];

    array_push($aItems, $admin_items_per_page);

    $aItems = array_unique($aItems);

    foreach ($aItems as $item) {
      $a[$item] =  trans('admin::views.Show items',  ['items' => $item]);
    }

    ksort($a);

    return $a;
  }

  public static function set($dataTableClassNamespace, $postParams = array(), $dataTableScript = 'data-table-script-reorder') {

    $dataTable = new self($dataTableClassNamespace, $postParams, $dataTableScript);

    return $dataTable->create();
  }

}
