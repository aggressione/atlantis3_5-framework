<?php

namespace Atlantis\Helpers;

use Atlantis\Models\Repositories\ModulesRepository;
use Illuminate\Support\Facades\DB;

/**
 *  This is a thin wrapper around Composer's Autoloader
 * to help us dynamically autoload classes from modules
 */
class Loader extends \Composer\Autoload\ClassLoader {

    private $active_modules;

    public function load($prefix, $paths) {

        $this->add($prefix, $paths);

        $this->register();
    }

    public function loadModules() {
        foreach ($this->getActiveModules() as $module) {
            $this->loadModule($module);
        }
    }

    public function loadModule($module) {
        /** if the config values from the DB are not empty **/
        if (!empty($module->namespace) && !empty($module->path)) {

            /** load the class Psr-0 style **/
            $this->load("{$module->namespace}\\", base_path() . "/modules/" . $module->path);

            /** register **/
            \App::register($module->provider);
        }
    }

    public function loadModuleSeed($seed, $path) {
        $this->load("{$seed}\\", base_path() . '/modules/' . $path);
    }

    public function publishModules() {

        foreach ($this->getActiveModules() as $module) {
            if (!$module->published_assets) {

                $tag = strtolower(Tools::stringToFolderName($module->name)) . '-assets';

                \Artisan::call('vendor:publish', [
                    '--tag' => $tag,
                    '--force' => "yes",
                    "--ansi" => "yes"
                ]);

                DB::table('modules')->where('id', '=', $module->id)->update(['published_assets' => true]);
                $this->active_modules = null;
            }
        }
    }

    public function getActiveModules() {
        if (empty($this->active_modules)) {

            $this->active_modules = ModulesRepository::getActiveModules();
        }

        return $this->active_modules;
    }
}
