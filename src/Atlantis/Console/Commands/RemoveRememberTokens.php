<?php

namespace Atlantis\Console\Commands;
use Atlantis\Models\User;

/**
 * User: gellezzz
 * Date: 12.10.23
 * Time: 21:27
 */

class RemoveRememberTokens extends \Illuminate\Console\Command {

    protected $signature = 'atlantis:remove-remember-tokens';
    protected $description = "Remove all user's remember tokek";

    public function handle() {

        foreach (User::cursor() as $user) {
            $user->remember_token = null;
            $user->update();
        }

        $this->comment('Done.');
    }

}
