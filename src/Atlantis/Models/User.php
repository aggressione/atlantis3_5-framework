<?php

namespace Atlantis\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable {
    use HasFactory, Notifiable, \Atlantis\Traits\Role;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'first_name', 'last_name', 'email', 'password', 'editor', 'language', 'widgets', 'last_login', 'ip'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles() {
        return $this->hasMany('Atlantis\Models\RolesUsers', 'user_id');

    }

    public function sendPasswordResetNotification($token) {

        $this->notify(new \Atlantis\Notifications\PasswordResetNotification($token, $this));

    }

    /**
     * Get the user's widget attribute.
     *
     * @param string $value
     * @return string
     */
    public function getWidgetsAttribute($value) {

        if (empty($value)) {
            return array();
        } else {
            return unserialize($value);
        }

    }

    /**
     * Set the user's widget attribute.
     *
     * @param string $value
     * @return string
     */
    public function setWidgetsAttribute($value) {

        if (empty($value)) {
            $this->attributes['widgets'] = serialize(array());
        } else {
            $this->attributes['widgets'] = serialize($value);
        }

    }
}
