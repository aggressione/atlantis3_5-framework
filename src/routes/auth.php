<?php

use Illuminate\Support\Facades\Route;
use Atlantis\Http\Controllers\Auth\RegisteredUserController;
use Atlantis\Http\Controllers\Auth\AuthenticatedSessionController;
use Atlantis\Http\Controllers\Auth\PasswordResetLinkController;
use Atlantis\Http\Controllers\Auth\NewPasswordController;
use Atlantis\Http\Controllers\Auth\EmailVerificationPromptController;
use Atlantis\Http\Controllers\Auth\VerifyEmailController;
use Atlantis\Http\Controllers\Auth\EmailVerificationNotificationController;
use Atlantis\Http\Controllers\Auth\ConfirmablePasswordController;

/**
Route::get('/register', [RegisteredUserController::class, 'create'])
                ->middleware('guest')
                ->name('register');

Route::post('/register', [RegisteredUserController::class, 'store'])
                ->middleware('guest');
*/

Route::get('/admin', [AuthenticatedSessionController::class, 'create'])
                ->middleware('guest')
                ->name('login');

Route::post('/admin', [AuthenticatedSessionController::class, 'store'])
                ->middleware('guest');

Route::get('/admin/password/email', [PasswordResetLinkController::class, 'create'])
                ->middleware('guest')
                ->name('password.request');

Route::post('/admin/password/email', [PasswordResetLinkController::class, 'store'])
                ->middleware('guest')
                ->name('password.email');

Route::get('/admin/password/reset/{token}', [NewPasswordController::class, 'create'])
                ->middleware('guest')
                ->name('password.reset');

Route::post('/admin/password/reset', [NewPasswordController::class, 'store'])
                ->middleware('guest')
                ->name('password.update');
/**
Route::get('/verify-email', [EmailVerificationPromptController::class, '__invoke'])
                ->middleware('auth')
                ->name('verification.notice');

Route::get('/verify-email/{id}/{hash}', [VerifyEmailController::class, '__invoke'])
                ->middleware(['auth', 'signed', 'throttle:6,1'])
                ->name('verification.verify');

Route::post('/email/verification-notification', [EmailVerificationNotificationController::class, 'store'])
                ->middleware(['auth', 'throttle:6,1'])
                ->name('verification.send');

Route::get('/confirm-password', [ConfirmablePasswordController::class, 'show'])
                ->middleware('auth')
                ->name('password.confirm');

Route::post('/confirm-password', [ConfirmablePasswordController::class, 'store'])
                ->middleware('auth');
*/

Route::get('/admin/logout', [AuthenticatedSessionController::class, 'destroy'])
                ->middleware('auth')
                ->name('logout');

