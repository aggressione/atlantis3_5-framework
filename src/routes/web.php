<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/** Login page protected route * */
Route::any(config('page-protected.route_login'), 'Atlantis\Controllers\SiteLoginController@index');
/** Logout page protected route * */
Route::any(config('page-protected.route_logout'), 'Atlantis\Controllers\SiteLoginController@logout');

require __DIR__ . '/auth.php';

Route::any('set-login-session', 'Atlantis\Controllers\LoginController@setLoginSession');
Route::any('get-logged-user', 'Atlantis\Controllers\LoginController@getLoggedUser');


/** Admin Dashboard Controller * */
/**
 * Note this call is a little bit different, basically this is the way to call
 * dynamic methods, however they have to be prefixed with "get<MethodName>"
 */
Route::group([
    'prefix' => 'admin',
    'name' => '',
    'icon' => 'icon icon-Speedometter',
    'tooltip' => 'Dashboard',
    'menu_item_url' => 'admin/dashboard',
    'identifier' => Atlantis\Controllers\Admin\AdminController::$_ID_DASHBOARD
], function () {
    Route::get('/dashboard', [\Atlantis\Controllers\Admin\DashboardController::class, 'getIndex']);
    //Route::controller('/dashboard', 'Atlantis\Controllers\Admin\DashboardController');
});

Route::group([
    'prefix' => 'admin',
    'name' => 'Pages',
    'menu_item_url' => 'admin/pages',
    'identifier' => Atlantis\Controllers\Admin\AdminController::$_ID_PAGES
], function () {
    Route::get('/pages', [\Atlantis\Controllers\Admin\PagesController::class, 'getIndex']);
    Route::get('/pages/add', [\Atlantis\Controllers\Admin\PagesController::class, 'getAdd']);
    Route::post('/pages/add', [\Atlantis\Controllers\Admin\PagesController::class, 'postAdd']);
    Route::get('/pages/edit/{id}/{version_id?}/{lang?}', [
        \Atlantis\Controllers\Admin\PagesController::class,
        'getEdit'
    ]);
    Route::post('/pages/edit/{id}', [\Atlantis\Controllers\Admin\PagesController::class, 'postEdit']);
    Route::any('/pages/patterns-per-page/{id}', [
        \Atlantis\Controllers\Admin\PagesController::class,
        'anyPatternsPerPage'
    ]);
    Route::any('/pages/remove-pattern/{page_id}/{pattern_id}/{type}', [
        \Atlantis\Controllers\Admin\PagesController::class,
        'anyRemovePattern'
    ]);
    Route::post('/pages/bulk-action', [\Atlantis\Controllers\Admin\PagesController::class, 'postBulkAction']);
    Route::post('/pages/bulk-action-version', [
        \Atlantis\Controllers\Admin\PagesController::class,
        'postBulkActionVersions'
    ]);
    Route::get('/pages/delete-page/{page_id}', [\Atlantis\Controllers\Admin\PagesController::class, 'getDeletePage']);
    Route::get('/pages/make-active-version/{id}/{version_id?}/{lang?}', [
        \Atlantis\Controllers\Admin\PagesController::class,
        'getMakeActiveVersion'
    ]);
    Route::post('/pages/clone-page/{id}/{lang?}', [
        \Atlantis\Controllers\Admin\PagesController::class,
        'postClonePage'
    ]);
    Route::get('/pages/delete-version/{id}/{version_id?}/{lang?}', [
        \Atlantis\Controllers\Admin\PagesController::class,
        'getDeleteVersion'
    ]);
    Route::any('/pages/related-images', [\Atlantis\Controllers\Admin\PagesController::class, 'anyRelatedImages']);
    Route::get('/pages/categories', [\Atlantis\Controllers\Admin\PagesController::class, 'getCategories']);
    Route::get('/pages/pages-list', [\Atlantis\Controllers\Admin\PagesController::class, 'getPagesList']);
    Route::get('/pages/data-status', [\Atlantis\Controllers\Admin\PagesController::class, 'getDataStatus']);
    //Route::controller('/pages', 'Atlantis\Controllers\Admin\PagesController');
    Route::get('/categories/add', [\Atlantis\Controllers\Admin\CategoriesController::class, 'getAdd']);
    Route::post('/categories/add', [\Atlantis\Controllers\Admin\CategoriesController::class, 'postAdd']);
    Route::get('/categories/edit/{id}', [\Atlantis\Controllers\Admin\CategoriesController::class, 'getEdit']);
    Route::post('/categories/edit/{id}', [\Atlantis\Controllers\Admin\CategoriesController::class, 'postEdit']);
    Route::get('/categories/delete/{id}', [\Atlantis\Controllers\Admin\CategoriesController::class, 'getDelete']);
    Route::post('/categories/bulk-action', [\Atlantis\Controllers\Admin\CategoriesController::class, 'postBulkAction']);
    //Route::controller('/categories', 'Atlantis\Controllers\Admin\CategoriesController');
});

Route::group([
    'prefix' => 'admin',
    'name' => 'Patterns',
    'menu_item_url' => 'admin/patterns',
    'identifier' => Atlantis\Controllers\Admin\AdminController::$_ID_PATTERNS
], function () {
    Route::get('/patterns', [\Atlantis\Controllers\Admin\PatternsController::class, 'getIndex']);
    Route::get('/patterns/add', [\Atlantis\Controllers\Admin\PatternsController::class, 'getAdd']);
    Route::post('/patterns/add', [\Atlantis\Controllers\Admin\PatternsController::class, 'postAdd']);
    Route::get('/patterns/edit/{id}/{version_id?}/{lang?}', [
        \Atlantis\Controllers\Admin\PatternsController::class,
        'getEdit'
    ]);
    Route::post('/patterns/edit/{id}', [\Atlantis\Controllers\Admin\PatternsController::class, 'postEdit']);
    Route::post('/patterns/clone-pattern/{id}/{lang?}', [
        \Atlantis\Controllers\Admin\PatternsController::class,
        'postClonePattern'
    ]);
    Route::get('/patterns/delete-pattern/{pattern_id}', [
        \Atlantis\Controllers\Admin\PatternsController::class,
        'getDeletePattern'
    ]);
    Route::post('/patterns/bulk-action-versions', [
        \Atlantis\Controllers\Admin\PatternsController::class,
        'postBulkActionVersions'
    ]);
    Route::post('/patterns/bulk-action', [\Atlantis\Controllers\Admin\PatternsController::class, 'postBulkAction']);
    Route::get('/patterns/make-active-version/{id}/{version_id?}/{lang?}', [
        \Atlantis\Controllers\Admin\PatternsController::class,
        'getMakeActiveVersion'
    ]);
    Route::get('/patterns/delete-version/{id}/{version_id?}/{lang?}', [
        \Atlantis\Controllers\Admin\PatternsController::class,
        'getDeleteVersion'
    ]);
});

Route::group([
    'prefix' => 'admin',
    'name' => 'Modules',
    'menu_item_url' => 'admin/modules',
    'identifier' => Atlantis\Controllers\Admin\AdminController::$_ID_MODULES
], function () {
    Route::get('/modules', [\Atlantis\Controllers\Admin\ModulesController::class, 'getIndex']);
    Route::get('/modules/activate-module/{id}', [
        \Atlantis\Controllers\Admin\ModulesController::class,
        'getActivateModule'
    ]);
    Route::get('/modules/deactivate-module/{id}', [
        \Atlantis\Controllers\Admin\ModulesController::class,
        'getDeactivateModule'
    ]);
    Route::get('/modules/update-modules-setup', [
        \Atlantis\Controllers\Admin\ModulesController::class,
        'getUpdateModulesSetup'
    ]);
    Route::post('/modules/install', [\Atlantis\Controllers\Admin\ModulesController::class, 'postInstall']);
    Route::post('/modules/uninstall/{id}', [\Atlantis\Controllers\Admin\ModulesController::class, 'postUninstall']);
    Route::post('/modules/update', [\Atlantis\Controllers\Admin\ModulesController::class, 'postUpdate']);
    Route::get('/modules/repository', [\Atlantis\Controllers\Admin\ModulesController::class, 'getRepository']);
    Route::post('/modules/download-install', [
        \Atlantis\Controllers\Admin\ModulesController::class,
        'postDownloadInstall'
    ]);
    Route::post('/modules/update-keys', [\Atlantis\Controllers\Admin\ModulesController::class, 'postUpdateKeys']);
    Route::post('/modules/create-page', [\Atlantis\Controllers\Admin\ModulesController::class, 'postCreatePage']);
});

Route::group([
    'prefix' => 'admin',
    'name' => 'Media',
    'menu_item_url' => 'admin/media',
    'identifier' => Atlantis\Controllers\Admin\AdminController::$_ID_MEDIA
], function () {

    Route::get('/media', [\Atlantis\Controllers\Admin\MediaController::class, 'getIndex']);
    Route::get('/media/media-add', [\Atlantis\Controllers\Admin\MediaController::class, 'getMediaAdd']);
    Route::post('/media/media-add', [\Atlantis\Controllers\Admin\MediaController::class, 'postMediaAdd']);
    Route::get('/media/media-edit/{id}', [\Atlantis\Controllers\Admin\MediaController::class, 'getMediaEdit']);
    Route::post('/media/media-edit/{id}', [\Atlantis\Controllers\Admin\MediaController::class, 'postMediaEdit']);
    Route::post('/media/media-recrop-version/{id}', [\Atlantis\Controllers\Admin\MediaController::class, 'postMediaRecropVersion']);
    Route::post('/media/bulk-action-media', [\Atlantis\Controllers\Admin\MediaController::class, 'postBulkActionMedia']);
    Route::get('/media/tags-to-image/{id}', [\Atlantis\Controllers\Admin\MediaController::class, 'getTagsToImage']);
    Route::post('/media/add-tags-to-multiple-images', [\Atlantis\Controllers\Admin\MediaController::class, 'postAddTagsToMultipleImages']);
    Route::get('/media/selected-images-to-new-gallery/{imageIDs}', [\Atlantis\Controllers\Admin\MediaController::class, 'getSelectedImagesToNewGallery']);
    Route::get('/media/add-image-to-gallery/{id}', [\Atlantis\Controllers\Admin\MediaController::class, 'getAddImageToGallery']);
    Route::post('/media/add-image-to-existing-gallery', [\Atlantis\Controllers\Admin\MediaController::class, 'postAddImageToExistingGallery']);
    Route::get('/media/media-delete/{id}', [\Atlantis\Controllers\Admin\MediaController::class, 'getMediaDelete']);
    Route::get('/media/gallery-add', [\Atlantis\Controllers\Admin\MediaController::class, 'getGalleryAdd']);
    Route::get('/media/gallery-edit/{id}', [\Atlantis\Controllers\Admin\MediaController::class, 'getGalleryEdit']);
    Route::post('/media/gallery-add', [\Atlantis\Controllers\Admin\MediaController::class, 'postGalleryAdd']);
    Route::post('/media/gallery-edit/{id}', [\Atlantis\Controllers\Admin\MediaController::class, 'postGalleryEdit']);
    Route::get('/media/gallery-delete/{id}', [\Atlantis\Controllers\Admin\MediaController::class, 'getGalleryDelete']);
    Route::post('/media/bulk-action-gallery', [\Atlantis\Controllers\Admin\MediaController::class, 'postBulkActionGallery']);
    Route::post('/media/add-img-to-gallery/{id}', [\Atlantis\Controllers\Admin\MediaController::class, 'postAddImgToGallery']);
    Route::post('/media/add-to-gallery', [\Atlantis\Controllers\Admin\MediaController::class, 'postAddToGallery']);
    Route::any('/media/all-galleries', [\Atlantis\Controllers\Admin\MediaController::class, 'anyAllGalleries']);
    //Route::controller('/media', 'Atlantis\Controllers\Admin\MediaController');
});

Route::group([
    'prefix' => 'admin',
    'parent' => '',
    'parent-icon' => 'icon icon-Settings',
    'name' => 'Users',
    'menu_item_url' => 'admin/users',
    'identifier' => Atlantis\Controllers\Admin\AdminController::$_ID_USERS
], function () {
    Route::get('/users', [\Atlantis\Controllers\Admin\UsersController::class, 'getIndex']);
    Route::get('/users/add', [\Atlantis\Controllers\Admin\UsersController::class, 'getAdd']);
    Route::post('/users/add', [\Atlantis\Controllers\Admin\UsersController::class, 'postAdd']);
    Route::get('/users/edit/{id}', [\Atlantis\Controllers\Admin\UsersController::class, 'getEdit']);
    Route::post('/users/edit/{id}', [\Atlantis\Controllers\Admin\UsersController::class, 'postEdit']);
    Route::get('/users/delete/{id}', [\Atlantis\Controllers\Admin\UsersController::class, 'getDelete']);
    Route::post('/users/bulk-action', [\Atlantis\Controllers\Admin\UsersController::class, 'postBulkAction']);
    //Route::controller('/users', 'Atlantis\Controllers\Admin\UsersController');
});

Route::group([
    'prefix' => 'admin',
    'parent' => '',
    'parent-icon' => 'icon icon-Settings',
    'name' => 'Roles',
    'menu_item_url' => 'admin/roles',
    'identifier' => Atlantis\Controllers\Admin\AdminController::$_ID_ROLES
], function () {
    Route::get('/roles', [\Atlantis\Controllers\Admin\RolesController::class, 'getIndex']);
    Route::get('/roles/add', [\Atlantis\Controllers\Admin\RolesController::class, 'getAdd']);
    Route::post('/roles/add', [\Atlantis\Controllers\Admin\RolesController::class, 'postAdd']);
    Route::get('/roles/edit/{id}', [\Atlantis\Controllers\Admin\RolesController::class, 'getEdit']);
    Route::post('/roles/edit/{id}', [\Atlantis\Controllers\Admin\RolesController::class, 'postEdit']);
    Route::get('/roles/delete/{id}', [\Atlantis\Controllers\Admin\RolesController::class, 'getDelete']);
    Route::post('/roles/bulk-action', [\Atlantis\Controllers\Admin\RolesController::class, 'postBulkAction']);
    //Route::controller('/roles', 'Atlantis\Controllers\Admin\RolesController');

});

Route::group([
    'prefix' => 'admin',
    'parent' => '',
    'parent-icon' => 'icon icon-Settings',
    'name' => 'Themes',
    'menu_item_url' => 'admin/themes',
    'identifier' => Atlantis\Controllers\Admin\AdminController::$_ID_THEMES
], function () {
    Route::get('/themes', [\Atlantis\Controllers\Admin\ThemesController::class, 'getIndex']);
    Route::get('/themes/details/{name}', [\Atlantis\Controllers\Admin\ThemesController::class, 'getDetails']);
    Route::post('/themes/activate-theme', [\Atlantis\Controllers\Admin\ThemesController::class, 'postActivateTheme']);
    Route::post('/themes/create-resource', [\Atlantis\Controllers\Admin\ThemesController::class, 'postCreateResource']);
    //Route::controller('/themes', 'Atlantis\Controllers\Admin\ThemesController');
});

Route::group([
    'prefix' => 'admin',
    'parent' => '',
    'parent-icon' => 'icon icon-Settings',
    'name' => 'Config',
    'menu_item_url' => 'admin/config',
    'identifier' => Atlantis\Controllers\Admin\AdminController::$_ID_CONFIG
], function () {
    Route::get('/config', [\Atlantis\Controllers\Admin\ConfigController::class, 'getIndex']);
    Route::post('/config/update', [\Atlantis\Controllers\Admin\ConfigController::class, 'postUpdate']);
    Route::post('/config/sync-files', [\Atlantis\Controllers\Admin\ConfigController::class, 'postSyncFiles']);
    Route::post('/config/sync-files-v2', [\Atlantis\Controllers\Admin\ConfigController::class, 'postSyncFilesv2']);
    Route::post('/config/invalidate-files', [\Atlantis\Controllers\Admin\ConfigController::class, 'postInvalidateFiles']);
    //Route::controller('/config', 'Atlantis\Controllers\Admin\ConfigController');
});

Route::group([
    'prefix' => 'admin',
    'parent' => '',
    'parent-icon' => 'icon icon-Settings',
    'name' => 'Trash',
    'icon' => '',
    'menu_item_url' => 'admin/trash',
    'identifier' => Atlantis\Controllers\Admin\AdminController::$_ID_TRASH
], function () {
    Route::get('/trash', [\Atlantis\Controllers\Admin\TrashController::class, 'getIndex']);
    Route::get('/trash/restore-page/{id}', [\Atlantis\Controllers\Admin\TrashController::class, 'getRestorePage']);
    Route::get('/trash/delete-page/{id}', [\Atlantis\Controllers\Admin\TrashController::class, 'getDeletePage']);
    Route::post('/trash/bulk-action-page', [\Atlantis\Controllers\Admin\TrashController::class, 'postBulkActionPage']);
    Route::get('/trash/restore-pattern/{id}', [\Atlantis\Controllers\Admin\TrashController::class, 'getRestorePattern']);
    Route::get('/trash/delete-pattern/{id}', [\Atlantis\Controllers\Admin\TrashController::class, 'getDeletePattern']);
    Route::get('/trash/bulk-action-pattern', [\Atlantis\Controllers\Admin\TrashController::class, 'postBulkActionPattern']);
    Route::get('/trash/empty', [\Atlantis\Controllers\Admin\TrashController::class, 'getEmpty']);
    Route::controller('/trash', 'Atlantis\Controllers\Admin\TrashController');
});

//Route::controller('admin/trash', 'Atlantis\Controllers\Admin\TrashController');

/** Route to generate Google compatible xml sitemap * */
Route::get('sitemap.xml', 'Atlantis\Controllers\SiteMapController@index');

/** Reserve route for datatables */
Route::post('datatable-proccessing/getdata', 'Atlantis\Controllers\DataTableResolver@resolve');
Route::post('datatable-proccessing/reorder-data', 'Atlantis\Controllers\DataTableResolver@reorderData');

/** Page with alternate lang specified * */
Route::any('{lang?}/{page?}', 'Atlantis\Controllers\PageController@index')
    ->where(["lang" => "[a-z]{2}", "page" => ".+"]);

/** Return rendered patterns by id or pattern func * */
Route::post('api/get-pattern-by-id', 'Atlantis\Helpers\Tools@getLazyPattern');
Route::post('api/get-lazy-pattern-func', 'Atlantis\Helpers\Tools@getLazyPatternFunc');
Route::post('api/get-pattern-attr', 'Atlantis\Helpers\Themes\ThemeTools@getPatternAdminAttributes');

/**
 * load modules routes
 */
foreach (config('modules-routes', []) as $mr) {
    require $mr;
}

/** Page with no lang specified * */
Route::any('/{page?}', 'Atlantis\Controllers\PageController@index')
    ->where("page", ".*");
//Route::any('/', 'Atlantis\Controllers\PageController@index');

